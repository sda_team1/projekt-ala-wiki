package pl.sda.db;

import org.junit.Test;
import pl.sda.Login;
import pl.sda.db.data.generated.tables.records.UsersRecord;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class LoginTest {

    @Test
    public void testdoPost() throws IOException, ServletException {
        // given
        UserRepository repository = new UserRepository();
        UsersRecord record = new UsersRecord();
        String email = "testemail";
        String password = "testPassword";
        record.setUtcoffset("testO");
        record.setFirstname("testFirstName");
        record.setLastname("testLastName");
        record.setEmail(email);
        record.setLogin("testLogin");
        record.setPassword(password);
        Integer generatedId = repository.insert(record);

        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        HttpSession mockSession = mock(HttpSession.class);
        RequestDispatcher mockDispatcher = mock(RequestDispatcher.class);
        //TODO
        // test przechodzi jesli podam uzytkownika bedacego juz w bazie
        // ale nie przechodzi jesli dodam nowego przed wywolaniem doPost
//        when(mockRequest.getParameter("email")).thenReturn(email);
//        when(mockRequest.getParameter("password")).thenReturn(password);
        when(mockRequest.getParameter("email")).thenReturn("jerzywojcik@gmail.com");
        when(mockRequest.getParameter("password")).thenReturn("jerzy");
        String indexPage = "index.jsp";
        String loginPage = "login.jsp";
        when(mockRequest.getRequestDispatcher(indexPage)).thenReturn(mockDispatcher);
        when(mockRequest.getRequestDispatcher(loginPage)).thenReturn(mockDispatcher);
        when(mockRequest.getSession()).thenReturn(mockSession);

        // when
        Login login = new Login();
        login.doPost(mockRequest, mockResponse);

        // then
        verify(mockSession).setAttribute("isLogin", "true");
        verify(mockRequest).getRequestDispatcher(indexPage);
        verify(mockDispatcher).forward(mockRequest, mockResponse);

        // clean
        repository.delete(generatedId);
    }

    @Test
    public void testFaildoPost() throws IOException, ServletException {
        // given
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        HttpSession mockSession = mock(HttpSession.class);
        RequestDispatcher mockDispatcher = mock(RequestDispatcher.class);
        when(mockRequest.getParameter("email")).thenReturn("notInDatabase");
        when(mockRequest.getParameter("password")).thenReturn("notInDatabase");
        String indexPage = "index.jsp";
        String loginPage = "login.jsp";
        when(mockRequest.getRequestDispatcher(indexPage)).thenReturn(mockDispatcher);
        when(mockRequest.getRequestDispatcher(loginPage)).thenReturn(mockDispatcher);
        when(mockRequest.getSession()).thenReturn(mockSession);

        // when
        Login login = new Login();
        login.doPost(mockRequest, mockResponse);

        // then
        verify(mockSession, never()).setAttribute(anyString(), anyString());
        verify(mockRequest).getRequestDispatcher(loginPage);
        verify(mockDispatcher).forward(mockRequest, mockResponse);
    }
}
