package pl.sda.db;

import org.junit.Assert;
import org.junit.Test;
import pl.sda.Register;
import pl.sda.db.data.generated.tables.records.UsersRecord;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RegisterTest {

    @Test
    public void testDoPost() throws ServletException, IOException {
        // given
        UserRepository repository = new UserRepository();
        UsersRecord record;
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        RequestDispatcher mockDispatcher = mock(RequestDispatcher.class);
        when(mockRequest.getParameter("firstName")).thenReturn("testFirstName");
        when(mockRequest.getParameter("lastName")).thenReturn("testLastName");
        String email = "testEmail";
        when(mockRequest.getParameter("email")).thenReturn(email);
        when(mockRequest.getParameter("login")).thenReturn("testLogin");
        when(mockRequest.getParameter("login")).thenReturn("testLogin");
        when(mockRequest.getParameter("password")).thenReturn("testPassword");
        when(mockRequest.getParameter("utcOffset")).thenReturn("testOffset");
        String indexPage = "index.jsp";
        when(mockRequest.getRequestDispatcher(indexPage)).thenReturn(mockDispatcher);

        // when
        Register register = new Register();
        register.doPost(mockRequest, mockResponse);

        // then
        Assert.assertNotNull(record = repository.getUserByEmail(email));
        verify(mockRequest).getRequestDispatcher(indexPage);
        verify(mockDispatcher).forward(mockRequest, mockResponse);

        // clean
        repository.delete(record.getId());
    }
}
