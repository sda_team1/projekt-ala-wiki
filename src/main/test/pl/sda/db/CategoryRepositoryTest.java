package pl.sda.db;

import org.junit.Assert;
import org.junit.Test;
import pl.sda.db.CategoryRepository;
import pl.sda.db.data.generated.tables.records.CategoryRecord;

import java.util.List;

public class CategoryRepositoryTest {

    @Test
    public void testInsert() {
        // given
        CategoryRepository repository =new CategoryRepository();
        CategoryRecord record = new CategoryRecord();
        record.setCategoryname("testName");

        // when
        Integer generatedId = repository.insert(record);

        // then
        Assert.assertNotNull(repository.getById(generatedId));

        // clean
        repository.delete(generatedId);
    }

    @Test
    public void testGetById() {
        // given
        CategoryRepository repository = new CategoryRepository();
        CategoryRecord record = new CategoryRecord();
        record.setCategoryname("testName");
        Integer generatedId = repository.insert(record);

        // when
        CategoryRecord result = repository.getById(generatedId);

        // then
        Assert.assertNotNull(result);

        // clean
        repository.delete(generatedId);
    }

    @Test
    public void testGetCategories() {
        // given
        CategoryRepository repository = new CategoryRepository();

        // when
        List<CategoryRecord> categories = repository.getCategories();

        // then
        Assert.assertNotNull(categories);
    }

    @Test
    public void testDelete() {
        // given
        CategoryRepository repository =new CategoryRepository();
        CategoryRecord record = new CategoryRecord();
        record.setCategoryname("testName");
        Integer generatedId = repository.insert(record);

        // when
        repository.delete(generatedId);

        // then
        Assert.assertNull(repository.getById(generatedId));

    }

    @Test
    public void testUpdate() {
        // given
        CategoryRepository repository =new CategoryRepository();
        CategoryRecord record = new CategoryRecord();
        record.setCategoryname("testName");
        Integer generatedId = repository.insert(record);
        String change = "CHANGED NAME";

        //when
        record.setId(generatedId);
        record.setCategoryname(change);
        repository.update(record);

        //then
        Assert.assertEquals(change,repository.getById(generatedId).getCategoryname());

        //clean
        repository.delete(generatedId);
    }
}
