package pl.sda.db;

import org.jooq.DSLContext;
import org.junit.Assert;
import org.junit.Test;
import pl.sda.db.data.generated.Tables;
import pl.sda.db.data.generated.tables.Article;
import pl.sda.db.data.generated.tables.records.ArticleRecord;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static pl.sda.db.DatabaseRepository.connection;
import static pl.sda.db.DatabaseRepository.jooq;

public class DatabaseAccessTest {

    @Test
    public void test1() throws SQLException {
        // wykoanie zapytani
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);

            // wskazanie tabelki z jakiej obywa sie zapytanie
            Article article = Tables.ARTICLE;

            // przykadlowe pobranie danych
            List<ArticleRecord> collect = ctx.selectFrom(article).fetch().stream().collect(Collectors.toList());

            // then
            Assert.assertFalse(collect.isEmpty());
            ArticleRepository db = new ArticleRepository();
            Assert.assertEquals(collect, db.getArticles());
        }
    }

}