package pl.sda.db;

import org.junit.Assert;
import org.junit.Test;
import pl.sda.EditPost;
import pl.sda.db.data.generated.tables.records.ArticleRecord;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class EditPostTest {

    @Test
    public void testDoPost() throws ServletException, IOException {
        // given
        ArticleRepository repository = new ArticleRepository();
        ArticleRecord record;
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        HttpSession mockSession = mock(HttpSession.class);
        RequestDispatcher mockDispatcher = mock(RequestDispatcher.class);
        String mockContent = "testContent";
        String mockTitle= "testTitle";
        String dispatchPage = "index.jsp";
        when(mockRequest.getParameter("articleTitle")).thenReturn(mockTitle);
        when(mockRequest.getParameter("content")).thenReturn(mockContent);
        when(mockRequest.getParameter("category")).thenReturn(String.valueOf(1));
        when(mockRequest.getRequestDispatcher(dispatchPage)).thenReturn(mockDispatcher);
        when(mockRequest.getSession()).thenReturn(mockSession);
        when(mockSession.getAttribute("idUser")).thenReturn(1);

        // when
        EditPost editPost = new EditPost();
        editPost.doPost(mockRequest,mockResponse); // aby to uruchomic nalezy metode doPost zmienic na public z protected

        // then
        Assert.assertNotNull(record = repository.getByTitle(mockTitle));
        verify(mockRequest).getRequestDispatcher(dispatchPage);
        verify(mockDispatcher,times(1)).forward(mockRequest,mockResponse);

        // clean
        repository.delete(record.getId());
    }

}
