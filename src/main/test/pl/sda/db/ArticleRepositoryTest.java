package pl.sda.db;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Test;
import pl.sda.db.data.generated.tables.records.ArticleRecord;

import java.util.List;

public class ArticleRepositoryTest {

    @Test
    public void test1() {
        // given
        ArticleRepository db = new ArticleRepository();

        // when
        List<ArticleRecord> articles = db.getArticles();

        // then
        Assert.assertNotNull(articles);
    }

    @Test
    public void testGetById() {
        // given
        ArticleRepository db = new ArticleRepository();
        Integer id = 9;

        // when
        ArticleRecord article = db.getById(id);

        // then
        Assert.assertEquals(id,article.getId());
    }

    @Test
    public void testGetByTitle() {
        // given
        ArticleRepository repository =new ArticleRepository();
        ArticleRecord record = new ArticleRecord();
        String title = "testTitle";
        record.setTitle(title);
        record.setContent("testContent");
        record.setIduser(1);
        record.setIdcategory(1);
        Integer generatedId = repository.insert(record);

        // when
        ArticleRecord result = repository.getByTitle(title);

        // then
        Assert.assertNotNull(result);

        //clean
        repository.delete(generatedId);
    }

    @Test
    public void testGetByCategory() {
        // given
        ArticleRepository db = new ArticleRepository();
        String category = "people";

        // when
        List<ArticleRecord> articles = db.getByCategory(category);

        // then
        Assert.assertNotNull(articles);
    }

    @Test
    public void testRemoveScriptTags() {
        // given
        ArticleRepository repository = new ArticleRepository();
        String content = "Ala ma kota.<script> stealpassword()</script>TEST";

        // when
        String result = repository.removeScriptTags(content);

        // then
        Assert.assertEquals("Ala ma kota.TEST",result);
    }

    @Test
    public void testRemoveScriptTagsHarder() {
        // given
        ArticleRepository repository = new ArticleRepository();
        String content = "Ala ma kota.<script src=\"someFile.js\"> stealpassword()</script>TEST";

        // when
        String result = repository.removeScriptTags(content);

        // then
        Assert.assertEquals("Ala ma kota.TEST",result);
    }

    @Test
    public void testInsert() {
        // given
        ArticleRepository repository =new ArticleRepository();
        ArticleRecord record = new ArticleRecord();
        record.setTitle("testTitle");
        record.setContent("testContent");
        record.setIduser(1);
        record.setIdcategory(1);

        // when
        Integer generatedId = repository.insert(record);

        // then
        Assert.assertNotNull(repository.getById(generatedId));

        // clean
        repository.delete(generatedId);
    }

    @Test
    public void testDelete() {
        // given
        ArticleRepository repository =new ArticleRepository();
        ArticleRecord record = new ArticleRecord();
        record.setTitle("testTITLE");
        record.setContent("testContent");
        record.setIduser(1);
        record.setIdcategory(1);
        Integer generatedId = repository.insert(record);

        // when
        repository.delete(generatedId);

        // then
        Assert.assertNull(repository.getById(generatedId));

    }

    @Test
    public void testUpdate() {
        // given
        ArticleRepository repository =new ArticleRepository();
        ArticleRecord record = new ArticleRecord();
        record.setTitle("testTITLE");
        record.setContent("testContent");
        record.setIduser(1);
        record.setIdcategory(1);
        Integer generatedId = repository.insert(record);
        String change = "CHANGED CONTENT";

        //when
        record.setId(generatedId);
        record.setContent(change);
        repository.update(record);

        //then
        Assert.assertEquals(change,repository.getById(generatedId).getContent());

        //clean
        repository.delete(generatedId);
    }

    @Test
    public void testGetPartContent() {
        ArticleRepository repository = new ArticleRepository();
        List<ArticleRecord> articles = repository.getArticles();
        for (ArticleRecord article : articles) {
            String content = article.getContent();
            Document document = Jsoup.parse(content);
            System.out.println(repository.getPartContent(article));
            System.out.println();
        }
    }
}