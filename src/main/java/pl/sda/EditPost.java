package pl.sda;

import pl.sda.db.ArticleRepository;
import pl.sda.db.data.generated.tables.records.ArticleRecord;
import pl.sda.db.data.generated.tables.records.UsersRecord;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "EditPost", value = "/editpost")
public class EditPost extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        ArticleRepository repository = new ArticleRepository();
        ArticleRecord record = new ArticleRecord();



        //TODO
        //update się wywala przy próbie zapisu, pogwałcenia unikalności klucza głównego


        if(request.getParameter("idAutor") == null){
            record.setIduser((Integer) request.getSession().getAttribute("idUser"));
            record.setTitle(request.getParameter("articleTitle"));
            record.setContent(request.getParameter("content"));
            record.setIdcategory(Integer.parseInt(request.getParameter("category")));
            repository.insert(record);
        } else{
            record = repository.getById(Integer.parseInt(request.getParameter("idArticle")));
           // record.setIduser(Integer.parseInt(request.getParameter("idAutor")));
            record.setTitle(request.getParameter("articleTitle"));
            record.setContent(request.getParameter("content"));
            record.setIdcategory(Integer.parseInt(request.getParameter("category")));
            repository.update(record);
        }
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
}
