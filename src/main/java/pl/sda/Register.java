package pl.sda;

import pl.sda.db.UserRepository;
import pl.sda.db.data.generated.tables.records.UsersRecord;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Register", value = "/register")
public class Register extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        UserRepository repository = new UserRepository();
        UsersRecord record = new UsersRecord();

        record.setFirstname(request.getParameter("firstName"));
        record.setLastname(request.getParameter("lastName"));
        record.setEmail(request.getParameter("email"));
        record.setLogin(request.getParameter("login"));
        record.setPassword(repository.encodePassword(request.getParameter("password")));
        record.setUtcoffset(request.getParameter("utcOffset"));

        try {
            repository.insert(record);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } catch (IllegalArgumentException e) {
            request.getRequestDispatcher("register.jsp").forward(request, response);
        }
    }
}
