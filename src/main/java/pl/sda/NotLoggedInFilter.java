package pl.sda;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebFilter(filterName = "NotLoggedInFilter" , urlPatterns = {"/editpost"})
public class NotLoggedInFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        // before

//        HttpSession session = ((HttpServletRequest)servletRequest).getSession();
//        if ("true".equals(session.getAttribute("isLogin"))) {
//            filterChain.doFilter(servletRequest,servletResponse);
//
//        } else {
//            PrintWriter out = servletResponse.getWriter();
//            servletResponse.setContentType("text/html");
//            out.println("<script type=\"text/javascript\">");
//            out.println("alert('Nie jestes zalogowany');");
//            out.println("</script>");
//            out.println("<meta http-equiv='refresh' content='1;URL=index.jsp'>");
//        }

        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {

    }
}
