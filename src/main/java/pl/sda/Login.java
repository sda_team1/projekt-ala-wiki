package pl.sda;

import org.jooq.Result;
import pl.sda.db.ArticleRepository;
import pl.sda.db.CategoryRepository;
import pl.sda.db.UserRepository;
import pl.sda.db.data.generated.tables.records.UsersRecord;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Login", value = "/login")
public class Login extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        String login = request.getParameter("email");
        String password = request.getParameter("password");

        UserRepository userRepository = new UserRepository();

        String sha256Password = userRepository.encodePassword(password);
        UsersRecord loginUser = userRepository.getUserByEmail(login);

        if (loginUser != null && loginUser.getPassword().equals(sha256Password)) {
            request.getSession().setAttribute("firstName", loginUser.getFirstname());
            request.getSession().setAttribute("lastName", loginUser.getLastname());
            request.getSession().setAttribute("accessLevel", loginUser.getAccesslevel());
            request.getSession().setAttribute("utcOffset", loginUser.getUtcoffset());
            request.getSession().setAttribute("idUser", loginUser.getId());
            request.getSession().setAttribute("isLogin", "true");

            /**
             * Zastapenie frowarda zadania typu POST na zwykle przkierowanie GET na storne glowna po zalogowaniu do systemu.
             *
             * request.getRequestDispatcher("index.jsp").forward(request, response);
             */
            response.sendRedirect(request.getServletContext().getContextPath() + "/");
        } else {
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }

    }
}
