package pl.sda;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

@WebServlet(name = "sendMail", value = "/mail")
public class SendMail extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
//        UserRepository repository = new UserRepository();
        String sendTo = request.getParameter("email");
        String wikiMailAddress = "sda.team1.wiki@gmail.com";
        String wikiLogin = "sda.team1.wiki";
        String wikiPassword = "admin1234!";
//        UsersRecord user = repository.getUserByEmail(sendTo);
        String mailSubject = "Zmiana hasła";// + user.getLogin();
        String randomPassword = "zmienTo";
        String mailText = "Nowe hasło to: " + randomPassword + "\nHasło wygenerowane losowo, zmień je jak najszybciej" ;
        String host = "smtp.gmail.com";

        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "587");
        properties.setProperty("mail.user", wikiLogin);
        properties.setProperty("mail.password", wikiPassword);
        Session session = Session.getDefaultInstance(properties);
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(wikiMailAddress));
            message.addRecipient(Message.RecipientType.TO,InternetAddress.parse(sendTo)[0]);
            message.setSubject(mailSubject);
            message.setText(mailText);
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        out.println("<script type=\"text/javascript\">");
            out.println("alert('Nowe haslo zostalo wyslane na twoj adres email');");
            out.println("</script>");
            out.println("<meta http-equiv='refresh' content='1;URL=index.jsp'>");
    }
}

