package pl.sda;

import pl.sda.db.UserRepository;
import pl.sda.db.data.generated.tables.records.UsersRecord;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "EditUser", value = "/edituser")
public class EditUser extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        UserRepository repository = new UserRepository();
        UsersRecord usersRecord = repository.getById(Integer.parseInt(request.getParameter("idUser")));

        if("blocked".equals(usersRecord.getAccesslevel())){
            usersRecord.setAccesslevel("creator");
            repository.storeUser(usersRecord);
        }else{
            usersRecord.setAccesslevel("blocked");
            repository.storeUser(usersRecord);
        }
        request.getRequestDispatcher("adminpage.jsp").forward(request, response);
    }
}
