package pl.sda.db;

import com.google.common.base.Strings;
import org.jooq.DSLContext;
import org.jooq.Result;
import pl.sda.db.data.generated.Tables;
import pl.sda.db.data.generated.tables.Users;
import pl.sda.db.data.generated.tables.records.UsersRecord;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class UserRepository extends DatabaseRepository {

    public Integer insert(UsersRecord record) {
        if (Strings.isNullOrEmpty(record.getFirstname())
                || Strings.isNullOrEmpty(record.getLastname())
                || Strings.isNullOrEmpty(record.getLogin())
                || Strings.isNullOrEmpty(record.getEmail())
                || Strings.isNullOrEmpty(record.getPassword())
                || Strings.isNullOrEmpty(record.getUtcoffset())) {
            throw new IllegalArgumentException("Podane dane są niepoprawne.");
        }

        return storeUser(record);
    }

    /**
     * @return pobranie wszystkich uzytkownikow
     */
    public List<UsersRecord> getUsers() {
        // wykonanie zapytania
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            Users users = Tables.USERS;
            return ctx.selectFrom(users).fetch().stream().collect(Collectors.toList());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public UsersRecord getById(Integer id) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            Users users = Tables.USERS;
            return ctx.selectFrom(users)
                    .where(users.ID.equal(id))
                    .fetchOne();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public UsersRecord getUserByEmail(String email) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            Users users = Tables.USERS;
            return ctx.selectFrom(users).where(users.EMAIL.equal(email)).fetchOne();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<UsersRecord> getByAccessLevel(String level) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            Users users = Tables.USERS;
            return ctx.selectFrom(users)
                    .where(users.ACCESSLEVEL.eq(level))
                    .fetch()
                    .stream().collect(Collectors.toList());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void update2(UsersRecord record) {
        if (Strings.isNullOrEmpty(record.getFirstname())
                || Strings.isNullOrEmpty(record.getLastname())
                || Strings.isNullOrEmpty(record.getLogin())
                || Strings.isNullOrEmpty(record.getEmail())
                || Strings.isNullOrEmpty(record.getPassword())
                || Strings.isNullOrEmpty(record.getUtcoffset())) {
            throw new IllegalArgumentException("Podane dane są niepoprawne.");
        }

        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            record.attach(ctx.configuration());
            record.store();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void update(UsersRecord record) {
        if (Strings.isNullOrEmpty(record.getFirstname())
                || Strings.isNullOrEmpty(record.getLastname())
                || Strings.isNullOrEmpty(record.getLogin())
                || Strings.isNullOrEmpty(record.getEmail())
                || Strings.isNullOrEmpty(record.getPassword())
                || Strings.isNullOrEmpty(record.getUtcoffset())) {
            throw new IllegalArgumentException("Podane dane są niepoprawne.");
        }

        storeUser(record);
    }

    public void delete(Integer id) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);

            Users users = Tables.USERS;

            ctx.delete(users)
                    .where(users.ID.eq(id))
                    .execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Integer storeUser(UsersRecord record) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);

            record.attach(ctx.configuration());
            record.store();
            record.refresh();
            return record.getId();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
