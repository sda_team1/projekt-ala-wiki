package pl.sda.db;

import com.google.common.base.Strings;
import org.jooq.DSLContext;
import pl.sda.db.data.generated.Tables;
import pl.sda.db.data.generated.tables.Category;
import pl.sda.db.data.generated.tables.records.CategoryRecord;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class CategoryRepository extends DatabaseRepository {


    public Integer insert(CategoryRecord record) {
        if (Strings.isNullOrEmpty(record.getCategoryname())) {
            throw new IllegalArgumentException("Podane dane są niepoprawne.");
        }
        return storeCategory(record);
    }

    public CategoryRecord getById(Integer id) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            Category category = Tables.CATEGORY;
            return ctx.selectFrom(category)
                    .where(category.ID.equal(id))
                    .fetchOne();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @return pobranie wszystkich kategorii
     */
    public List<CategoryRecord> getCategories() {
        // wykonanie zapytania
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            Category category = Tables.CATEGORY;
            return ctx.selectFrom(category).fetch().stream().collect(Collectors.toList());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void update(CategoryRecord record) {
        if (Strings.isNullOrEmpty(record.getCategoryname())) {
            throw new IllegalArgumentException("Podane dane są niepoprawne.");
        }

        storeCategory(record);
    }

    public void delete(Integer id) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);

            Category category = Tables.CATEGORY;

            ctx.delete(category)
                    .where(category.ID.eq(id))
                    .execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Integer storeCategory(CategoryRecord record) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);

            record.attach(ctx.configuration());
            record.store();
            record.refresh();
            return record.getId();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
