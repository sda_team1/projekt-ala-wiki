package pl.sda.db;

import com.google.common.base.Strings;
import org.jooq.DSLContext;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import pl.sda.db.data.generated.Tables;
import pl.sda.db.data.generated.tables.Article;
import pl.sda.db.data.generated.tables.Category;
import pl.sda.db.data.generated.tables.records.ArticleRecord;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class ArticleRepository extends DatabaseRepository {

    public Integer insert(ArticleRecord record) {
        if (Strings.isNullOrEmpty(record.getTitle())
                || Strings.isNullOrEmpty(record.getContent())
                || record.getIduser() == null
                || record.getIdcategory() == null) {
            throw new IllegalArgumentException("Podane dane są niepoprawne.");
        }
        return storeArticle(record);

    }

    /**
     * @return pobranie listy artykułów
     */
    public List<ArticleRecord> getArticles() {
        // wykonanie zapytania
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);

            // wskazanie tabelki z jakiej obywa sie zapytanie
            Article article = Tables.ARTICLE;

            // przykładowe pobranie danych
            return ctx.selectFrom(article).orderBy(article.TITLE).fetch().stream().collect(Collectors.toList());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public ArticleRecord getById(Integer id) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);

            Article article = Tables.ARTICLE;

            return ctx.selectFrom(article)
                    .where(article.ID.eq(id))
                    .fetchOne();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public ArticleRecord getByTitle(String title) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);

            Article article = Tables.ARTICLE;

            return ctx.selectFrom(article)
                    .where(article.TITLE.eq(title))
                    .fetchAny();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<ArticleRecord> getByCategory(String articleCategory) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);

            Article article = Tables.ARTICLE;
            Category category = Tables.CATEGORY;


            return ctx.select()
                    .from(article)
                    .join(category)
                    .on(article.IDCATEGORY.eq(category.ID))
                    .where(category.CATEGORYNAME.eq(articleCategory))
                    .orderBy(article.TITLE)
                    .fetch()
                    .into(article);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void update(ArticleRecord record) {
        if (record.getTitle() == null || record.getContent() == null || record.getIduser() == null) {
            throw new IllegalArgumentException("Podane dane są niepoprawne.");
        }
        storeArticle(record);
    }

    public void delete(Integer id) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);

            Article article = Tables.ARTICLE;

            ctx.delete(article)
                    .where(article.ID.eq(id))
                    .execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Integer storeArticle(ArticleRecord record) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);

            record.attach(ctx.configuration());
            record.store();
            record.refresh();
            return record.getId();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public String getPartContent(ArticleRecord record) {
        int partLength = 150;
        String title = record.getTitle();
        Document document = Jsoup.parse(record.getContent());
        String result = document.body().text();
        if (result.contains(title)) {
            result = result.substring(result.indexOf(title) + title.length());
        }
        int titlePosition = result.indexOf(title);
        if (titlePosition == -1) {
            titlePosition = 0;
        }
        if (partLength + titlePosition > result.length()) {
            partLength = result.length() - titlePosition;
        }
//        int lastPeriod = result.substring(titlePosition + partLength).indexOf('.');
//        if (lastPeriod == -1) {
//            lastPeriod = 0;
//        }
        return result.substring(titlePosition, titlePosition + partLength) + "...";// + lastPeriod + 1);
    }

}
