/*
 * This file is generated by jOOQ.
*/
package pl.sda.db.data.generated;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;

import pl.sda.db.data.generated.tables.Article;
import pl.sda.db.data.generated.tables.Category;
import pl.sda.db.data.generated.tables.SqliteSequence;
import pl.sda.db.data.generated.tables.SqliteStat1;
import pl.sda.db.data.generated.tables.Users;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class DefaultSchema extends SchemaImpl {

    private static final long serialVersionUID = 194704595;

    /**
     * The reference instance of <code></code>
     */
    public static final DefaultSchema DEFAULT_SCHEMA = new DefaultSchema();

    /**
     * The table <code>article</code>.
     */
    public final Article ARTICLE = pl.sda.db.data.generated.tables.Article.ARTICLE;

    /**
     * The table <code>category</code>.
     */
    public final Category CATEGORY = pl.sda.db.data.generated.tables.Category.CATEGORY;

    /**
     * The table <code>sqlite_sequence</code>.
     */
    public final SqliteSequence SQLITE_SEQUENCE = pl.sda.db.data.generated.tables.SqliteSequence.SQLITE_SEQUENCE;

    /**
     * The table <code>sqlite_stat1</code>.
     */
    public final SqliteStat1 SQLITE_STAT1 = pl.sda.db.data.generated.tables.SqliteStat1.SQLITE_STAT1;

    /**
     * The table <code>users</code>.
     */
    public final Users USERS = pl.sda.db.data.generated.tables.Users.USERS;

    /**
     * No further instances allowed
     */
    private DefaultSchema() {
        super("", null);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Table<?>> getTables() {
        List result = new ArrayList();
        result.addAll(getTables0());
        return result;
    }

    private final List<Table<?>> getTables0() {
        return Arrays.<Table<?>>asList(
            Article.ARTICLE,
            Category.CATEGORY,
            SqliteSequence.SQLITE_SEQUENCE,
            SqliteStat1.SQLITE_STAT1,
            Users.USERS);
    }
}
