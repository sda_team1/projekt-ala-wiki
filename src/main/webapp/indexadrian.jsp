<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 02.08.2018
  Time: 20:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <meta charset="UTF-8">
</head>
<body>
<div class="container">
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills float-right">
                <li class="nav-item">
                    <a class="nav-link active" href="index.jsp">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="about.jsp">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="contact.jsp">Contact</a>
                </li>
            </ul>
        </nav>
        <h3 class="text-muted" >Wiki</h3>
    </div>

    <div class="jumbotron">
        <h1 class="display-3"><b>Zapraszamy na Wiki</b></h1>
        <p class="lead">Witajcie w naszej bibliotece artykułów. Znajdziecie tu artykuły z rozmaitych dziedzin.
            Pomóżcie nam rozwijać naszą bazę wiedzy. Projekt stworzył zespół z kursu SDA "Java od podstaw". Twórcy Jarosław Waszczuk, Jerzy Wójcik, Dominik Gęśla, Adrian Biernacki.</p>
        <p><a class="btn btn-lg btn-success" href="login.jsp" role="button">Login</a></p>
    </div>

    <div class="row marketing">
        <div class="col-lg-6">
            <h4>Ciekawy artykuł</h4>
            <p>Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum.</p>

            <h4>Często odwiedzany artykuł</h4>
            <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet
                fermentum.</p>

            <h4>Ulubiony artykuł</h4>
            <p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
        </div>

        <div class="col-lg-6">
            <h4>Zwierzęta</h4>
            <p>Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum.</p>

            <h4>Technika</h4>
            <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet
                fermentum.</p>

            <h4>Ciekawostki</h4>
            <p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
        </div>
    </div>

    <footer class="footer">
        <p>&copy; Company 2018</p>
    </footer>

</div>
</body>
</html>
