<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <meta charset="UTF-8">
    <style type="text/css">
        * {
            background-image: none !important;
        }
    </style>
</head>
<body>
<div class="container">
    <c:set var="pageName" value="article"/>
    <jsp:include page="menu.jsp">
        <jsp:param name="pageName" value="${pageName}"/>
    </jsp:include>

    <c:set var="isLogin" value="<%=request.getSession().getAttribute("isLogin")%>"/>
    <c:set var="accessLevel" value="<%=request.getSession().getAttribute("accessLevel")%>"/>
    <c:set var="idUser" value="<%=request.getSession().getAttribute("idUser")%>"/>
    <c:set value="<%=request.getParameter("id") %>" var="id"/>
    <c:set value="${sessionScope.get('ArticleRepository').getById(id)}" var="article"/>
    <c:set value="${sessionScope.get('CategoryRepository').getById(article.idcategory)}" var="category"/>
    <c:set value="${sessionScope.get('UserRepository').getById(article.iduser)}" var="user"/>

    <div class="jumbotron text-center">
        <h1 class="display-4"><strong>${article.title}</strong></h1>
        <p class="lead">${category.categoryname}</p>
        <c:if test="${isLogin eq 'true'}">
            <c:if test="${(article.iduser eq idUser) or (accessLevel eq 'admin')}">
                <a href="editpost.jsp?id=${id}">
                    <button class="btn btn-lg btn-warning">Edytuj artykuł</button>
                </a>
            </c:if>
        </c:if>
    </div>
    <div class="container" style="margin-top:30px">
        <div class="row">
            <div class="card">
                <div class="card-body">
                    ${article.content}
                </div>
                <div class="card-footer"><h5>Autor: ${user.firstname} ${user.lastname}</h5>
                    utworzono: ${article.createdate}, zmodyfikowano: ${article.lastchangedate}</div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <p>&copy; SDA_Team1</p>
    </footer>
</div>
</body>
</html>