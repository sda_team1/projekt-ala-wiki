<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="Portal a'la Wiki zrealizowany przgrupę SDA_Team1"/>
    <meta name="keywords" content="ala wiki"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <meta charset="UTF-8">
</head>
<body>
<div class="container">
    <c:set var="pageName" value="index" />
    <jsp:include page="menu.jsp">
        <jsp:param name="pageName" value="${pageName}"/>
    </jsp:include>

        <div class="jumbotron text-center">
            <h1 class="display-4"><strong>Strona logowania</strong></h1>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 offset-lg-3 offset-md-3">
            <form action="<%= application.getContextPath() %>/login" method="post">
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp"
                           placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                </div>
                <div>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>

            <form action="resetpass.jsp" method="post">
                <button class="btn btn-info" type="submit" name="passwordrecovery" id="passwordrecovery"
                        value="passwordrecovery">Zresetuj hasło
                </button>
            </form>
        </div>
    </div>
    <footer class="footer">
        <p>&copy; SDA_Team1</p>
    </footer>
</div>
</body>
</html>