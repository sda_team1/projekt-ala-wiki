<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <meta charset="UTF-8">
</head>
<body>
<div class="container">
    <c:set var="pageName" value="about"/>
    <jsp:include page="menu.jsp">
        <jsp:param name="pageName" value="${pageName}"/>
    </jsp:include>
    <div class="jumbotron">
        <h1 class="display-3"><b>Dowiedz się więcej o Wiki</b></h1>
        <br>
        <br>
        <br>
        <p class="lead"><b><i>Wiki</i></b> to projekt internetowego źródła artykułów o „otwartej treści”. Tworzymy
            największy zbiór artykułów w internecie. Wiki tworzy spontanicznie rozwijająca się społeczność, do której każdy może w dowolnej chwili dołączyć i
            śmiało modyfikować strony, pod warunkiem, że akceptuje zasady funkcjonowania Wiki. Społeczność ta w dużym
            stopniu sama ustala, w jakim kierunku podąża to przedsięwzięcie. Zawartość Wiki nie jest kontrolowana przez
            „radę redakcyjną” ani „zespół Wiki”. Możliwość samodzielnego edytowania Wiki, a także wycofywania
            błędnych zmian, zapewnia oprogramowanie. Projekt stworzył zespół z kursu SDA "Java od podstaw". Twórcy
            Jarosław Waszczuk, Jerzy Wójcik, Dominik Gęśla, Adrian Biernacki.
            <br>
            Materiały z Wikipedii można wykorzystywać pod określonymi warunkami, przewidzianymi przez twórców</p>
        <br>
        <c:if test="${isLogin eq 'true'}">
            <a href="editpost.jsp">
                <button type="button" class="btn btn-warning">Dodaj artykuł</button>
            </a>
        </c:if>
        <c:if test="${empty isLogin}">
            <a href="login.jsp">
                <button type="button" class="btn btn-success">Zaloguj</button>
            </a>
        </c:if>
    </div>
    <jsp:include page="footer.jsp"/>
</div>
</body>
</html>