<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="Portal a'la Wiki zrealizowany przgrupę SDA_Team1"/>
    <meta name="keywords" content="ala wiki"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <meta charset="UTF-8">
</head>
<body>
<div class="container">  <!-- container -->
    <c:set var="pageName" value="nazwa_podstrony" />  <%-- TU WYPELNIC NAZWE PODSTRONY --%>
    <jsp:include page="menu.jsp">
        <jsp:param name="pageName" value="${pageName}"/>
    </jsp:include>
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Tytuł podstrony</h1>        <%-- TU DODAC TYTUL PODSTRONY --%>
            <p class="lead">Krótki opis podstrony. Jedna linijka max.</p>   <%-- TU UZUPELNIC KROTKI OPIS --%>
        </div>
    </div>
    <%-- Zawartość strony --%>


    <%-- /Zawartość strony --%>
    <jsp:include page="footer.jsp"/>
</div> <!-- /container -->
</body>
</html>