<%@ page import="pl.sda.db.ArticleRepository" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="Portal a'la Wiki zrealizowany przez grupę SDA_Team1"/>
    <meta name="keywords" content="ala wiki"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"/>
    <meta charset="UTF-8">
</head>
<body>
<div class="container">
    <c:set var="pageName" value="index"/>
    <c:set var="isLogin" value="<%=request.getSession().getAttribute("isLogin")%>"/>
    <jsp:include page="menu.jsp">
        <jsp:param name="pageName" value="${pageName}"/>
    </jsp:include>
    <div class="jumbotron">
        <h1 class="display-3"><strong>SDA_Team1</strong></h1>
        <p class="lead">Portal na "modłę" Wiki.<br>Zrealizowany podczas kursu "Java od Podstaw" w Software Development
            Academy przez grupę SDA_Team1 w ramach projektu na zajęciach z
            Servletów i JSP prowadzonych przez Henryka Bendera<br>Crew: Adrian Biernacki, Dominik Gęśla, Jarosław
            Waszczuk, Jerzy Wójcik</p>
        <c:if test="${isLogin eq 'true'}">
            <a href="editpost.jsp">
                <button type="button" class="btn btn-warning">Dodaj artykuł</button>
            </a>
        </c:if>
        <c:if test="${empty isLogin}">
            <a href="register.jsp">
                <p><a class="btn btn-lg btn-success" href="register.jsp" role="button">Dołącz już teraz!</a></p>
            </a>
        </c:if>
    </div>

    <div class="row marketing">

        <c:set var="rand"><%= java.lang.Math.round(java.lang.Math.random() * 15)%>
        </c:set>
        <div class="col-lg-6">
            <c:set value="${sessionScope.get('ArticleRepository').getById((rand mod 15) +9)}" var="art"/>
            <h4><a href="article.jsp?id=${art.id}">${art.title}</a></h4>
            <p>${sessionScope.get('ArticleRepository').getPartContent(art)}</p>
        </div>

        <c:set var="rand"><%= java.lang.Math.round(java.lang.Math.random() * 15)%>
        </c:set>
        <div class="col-lg-6">
            <c:set value="${sessionScope.get('ArticleRepository').getById((rand mod 15) +9)}" var="art"/>
            <h4><a href="article.jsp?id=${art.id}">${art.title}</a></h4>
            <p>${sessionScope.get('ArticleRepository').getPartContent(art)}</p>
        </div>

        <c:set var="rand"><%= java.lang.Math.round(java.lang.Math.random() * 15)%>
        </c:set>
        <div class="col-lg-6">
            <c:set value="${sessionScope.get('ArticleRepository').getById((rand mod 15) +9)}" var="art"/>
            <h4><a href="article.jsp?id=${art.id}">${art.title}</a></h4>
            <p>${sessionScope.get('ArticleRepository').getPartContent(art)}</p>
        </div>

        <c:set var="rand"><%= java.lang.Math.round(java.lang.Math.random() * 15)%>
        </c:set>
        <div class="col-lg-6">
            <c:set value="${sessionScope.get('ArticleRepository').getById((rand mod 15) +9)}" var="art"/>
            <h4><a href="article.jsp?id=${art.id}">${art.title}</a></h4>
            <p>${sessionScope.get('ArticleRepository').getPartContent(art)}</p>
        </div>
    </div>
    <jsp:include page="footer.jsp"/>
</div> <!-- /container -->
</body>
</html>
