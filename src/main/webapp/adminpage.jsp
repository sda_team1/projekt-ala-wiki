<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <meta charset="UTF-8">
</head>
<style>
    .col-sm-6 {
        padding-left: 20px;
        padding-right: 20px;
        padding-bottom: 10px;
    }
</style>
<body>
<div class="container">
    <c:set var="pageName" value="admin"/>
    <jsp:include page="menu.jsp">
        <jsp:param name="pageName" value="${pageName}"/>
    </jsp:include>

    <c:set value="${sessionScope.get('ArticleRepository').getArticles()}" var="articles"/>
    <c:set value="${sessionScope.get('UserRepository').getUsers()}" var="users"/>

    <div class="jumbotron text-center">
        <h1 class="display-4"><strong>Administracja</strong></h1>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header bg-success text-white">Zarejestrowani użytkownicy</div>
                <div class="card-body bg-light">
                    <table class="table">
                        <c:forEach items="${users}" var="tmpUsers">
                            <tr>
                                <td>${tmpUsers.firstname} ${tmpUsers.lastname}</td>
                                <td>${tmpUsers.accesslevel}</td>
                                <td>
                                    <c:if test="${tmpUsers.accesslevel eq 'blocked'}">
                                        <form action="<%= application.getContextPath() %>/edituser"
                                              method="post">
                                        <input type="hidden" name="idUser" value="${tmpUsers.id}">
                                        <button class="btn btn-warning" type="submit">Odblokuj</button>
                                        </form>
                                    </c:if>
                                    <c:if test="${tmpUsers.accesslevel != 'blocked'}">
                                        <form action="<%= application.getContextPath() %>/edituser"
                                              method="post">
                                        <input type="hidden" name="idUser" value="${tmpUsers.id}">
                                        <button class="btn btn-danger" type="submit">Zablokuj</button>
                                        </form>
                                    </c:if>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header bg-success text-white">Lista artykułów</div>
                <div class="card-body bg-light">
                    <table class="table">
                        <c:forEach items="${articles}" var="tmpArticles">
                        <tr>
                            <c:set value="${sessionScope.get('CategoryRepository').getById(tmpArticles.idcategory)}"
                                   var="category"/>
                            <td>${tmpArticles.title}</td>
                            <td>${category.categoryname}</td>
                            <td><a href="editpost.jsp?id=${tmpArticles.id}"
                                <button class="btn btn-success">Edytuj</button>
                            </td>
                        </tr>
                    </c:forEach></div>
                </table>
            </div>
        </div>
    </div>
    <footer class="footer">
        <p>&copy; SDA_Team1</p>
    </footer>
</div>

</body>
</html>