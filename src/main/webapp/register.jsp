<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="Portal a'la Wiki zrealizowany przgrupę SDA_Team1"/>
    <meta name="keywords" content="ala wiki"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <meta charset="UTF-8">
</head>
<body>
<div class="container">
    <c:set var="pageName" value="register" />
    <jsp:include page="menu.jsp">
        <jsp:param name="pageName" value="${pageName}"/>
    </jsp:include>
    <div class="jumbotron text-center">
        <h1 class="display-4"><strong>Rejestracja użytkownika</strong></h1>
        <p>Tutaj możesz się zarejestrować na portalu</p>
    </div>
    <form action="<%= application.getContextPath() %>/register" method="post">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <h3>First Name</h3>
                    <input type="text" class="form-control" name="firstName" placeholder="First name"><br>
                </div>
                <div class="col-lg-4">
                    <h3>Last Name</h3>
                    <input type="text" class="form-control" name="lastName" placeholder="Last name"><br>
                </div>
                <div class="col-lg-4">
                    <h3>Email address</h3>
                    <input type="email" class="form-control" name="email" placeholder="Email adress"><br>
                </div>
                <div class="col-lg-4">
                    <h3>Login</h3>
                    <input type="text" class="form-control" name="login" placeholder="Login" required/><br>
                </div>
                <div class="col-lg-4">
                    <h3>Password</h3>
                    <input type="password" class="form-control" name="password" placeholder="Password" required><br>
                </div>
                <div class="col-lg-4">
                    <h3>Timezone UTC</h3>
                    <select class="form-control" name="utcOffset">
                        <option value="-12:00">-12:00</option>
                        <option value="-11:00">-11:00</option>
                        <option value="-10:00">-10:00</option>
                        <option value="-09:30">-10:00</option>
                        <option value="-09:00">-09:00</option>
                        <option value="-08:00">-08:00</option>
                        <option value="-07:00">-07:00</option>
                        <option value="-06:00">-06:00</option>
                        <option value="-05:00">-05:00</option>
                        <option value="-04:00">-04:00</option>
                        <option value="-03:30">-04:00</option>
                        <option value="-03:00">-03:00</option>
                        <option value="-02:00">-02:00</option>
                        <option value="-01:00">-01:00</option>
                        <option value="+00:00" selected>+00:00</option>
                        <option value="+01:00">+01:00</option>
                        <option value="+02:00">+02:00</option>
                        <option value="+03:00">+03:00</option>
                        <option value="+03:30">+03:30</option>
                        <option value="+04:00">+04:00</option>
                        <option value="+04:30">+04:30</option>
                        <option value="+05:00">+05:00</option>
                        <option value="+05:30">+05:30</option>
                        <option value="+05:45">+05:45</option>
                        <option value="+06:00">+06:00</option>
                        <option value="+06:30">+06:30</option>
                        <option value="+07:00">+07:00</option>
                        <option value="+08:00">+08:00</option>
                        <option value="+09:00">+09:00</option>
                        <option value="+09:30">+09:30</option>
                        <option value="+10:00">+10:00</option>
                        <option value="+10:30">+10:30</option>
                        <option value="+11:00">+11:00</option>
                        <option value="+11:30">+11:30</option>
                        <option value="+12:00">+12:00</option>
                        <option value="+12:45">+12:45</option>
                        <option value="+13:00">+13:00</option>
                        <option value="+14:00">+14:00</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-success btn-block" >Submit</button>
            </div>
        </div>
    </form>
    <footer class="footer">
        <p>&copy; SDA_Team1</p>
    </footer>
</div>
</body>
</html>