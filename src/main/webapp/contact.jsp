<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <meta charset="UTF-8">
</head>
<body>
<div class="container">
    <c:set var="pageName" value="contact" />
    <jsp:include page="menu.jsp">
        <jsp:param name="pageName" value="${pageName}"/>
    </jsp:include>
    <div class="jumbotron">
        <h1 align="center" class="display-3" ><b>Kontakt</b></h1>
        <br>
        <br>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script><div style='overflow:hidden;height:400px;width:400px;'><div id='gmap_canvas' style='height:400px;width:400px;'></div><div><small><a href="https://embedgooglemaps.com/pl/">embedgooglemaps PL</a></small></div><div><small><a href="https://ww.buywebtrafficexperts.com">BWT Experts</a></small></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div><script type='text/javascript'>function init_map(){var myOptions = {zoom:10,center:new google.maps.LatLng(51.2464536,22.568446300000005),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(51.2464536,22.568446300000005)});infowindow = new google.maps.InfoWindow({content:'<strong>Wiki</strong><br>Lublin, Poland<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
                </div>
                <div class="col-sm">
                    <font size="7"><i class="fas fa-phone-volume" style="color:blue"></i> 666-666-666</font>
                    <br>
                    <br>
                    <br>
                    <font size="7"><i class="fas fa-envelope" style="color:blue"></i> wiki@info.pl</font>
                    <br>
                    <br>
                    <br>
                    <font size="7"><i class="fas fa-building" style="color:blue"></i> Lublin</font>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="footer.jsp"/>
</div>
</body>
</html>