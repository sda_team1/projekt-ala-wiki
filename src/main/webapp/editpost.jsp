<%@ page import="pl.sda.db.ArticleRepository" %>
<%@ page import="pl.sda.db.CategoryRepository" %>
<%@ page import="pl.sda.db.data.generated.tables.records.CategoryRecord" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="Portal a'la Wiki zrealizowany przez grupę SDA_Team1"/>
    <meta name="keywords" content="ala wiki"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <style type="text/css">
        .note-popover {
            display: none !important;
            visibility: hidden;
        }
    </style>
</head>
<body>
<div class="container">  <!-- container -->
    <c:set var="id" value="<%=request.getParameter("id")%>"/>
    <c:set var="pageName" value="editpost"/> <%-- TU WYPELNIC NAZWE PODSTRONY --%>
    <jsp:include page="menu.jsp">
        <jsp:param name="pageName" value="${pageName}"/>
    </jsp:include>
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Edytor artykułów</h1> <%-- TU DODAC TYTUL PODSTRONY --%>
            <p class="lead">Tutaj możesz stworzyć nowe artykuły i edytować
                istniejące.</p> <%-- TU UZUPELNIC KROTKI OPIS --%>
        </div>
    </div>
    <!--
    //TODO
    podpiąć pod zapisywanie w bazie
    doszlifować wygląd
    -->
    <c:set value="${sessionScope.get('ArticleRepository').getById(id)}" var="article"/>
    <form action="<%= application.getContextPath() %>/editpost" method="post">
        <div class="form-group">
            <label for="articleTitle">Tytuł artykułu:</label>
            <input type="text" class="form-control" id="articleTitle" name="articleTitle"
                   aria-describedby="articleTitleHelp"
                   placeholder="Wpisz tytuł..." value="${article.title}">
            <small id="articleTitleHelp" class="form-text text-muted">Powyżej wpisz tytuł artykułu...</small>
        </div>

        <div class="form-group">
            <label for="category">Kategoria:</label>
            <select class="form-control" id="category" name="category" aria-describedby="categoryHelp">
                <c:forEach items="${sessionScope.get('CategoryRepository').getCategories()}" var="category">
                    <c:if test="${article.idcategory eq category.id}">
                        <option value="<c:out value="${category.id}" />" selected><c:out
                                value="${category.categoryname}"/></option>
                    </c:if>
                    <c:if test="${article.idcategory != category.id}">
                        <option value="<c:out value="${category.id}" />"><c:out
                                value="${category.categoryname}"/></option>
                    </c:if>
                </c:forEach>
            </select>
            <small id="categoryHelp" class="form-text text-muted">Powyżej wybierz kategorię...</small>
        </div>

        <div class="form-group">
            <label for="summernote">Artykuł:</label>
            <textarea id="summernote" name="content"
                      aria-describedby="editorHelp"
                      placeholder="Tu wpisz treść artykułu...">${article.content}</textarea>
            <small id="editorHelp" class="form-text text-muted">Powyżej wpisz treść artykułu...</small>
        </div>
        <%--//TODO--%>
        <%--//żeby nie zmieniało pierwotnego autora--%>
        <%--powinno byc dodatkowe pole z id kto edytował--%>
        <c:if test="${article.iduser != null}">
            <input type="hidden" name="idArticle" value="${article.id}">
            <input type="hidden" name="idAutor" value="${article.iduser}">
        </c:if>
        <button type="submit" class="btn btn-primary">Zapisz</button>
    </form>


    <%--<div id="summernote"><p>Edytor artykułów Zażółć gęślą jaźń</p></div>--%>

    <jsp:include page="footer.jsp"/>
</div> <!-- /container -->
<script>
    $(document).ready(function () {
        $('#summernote').summernote({
            height: 500,
            minHeight: null,
            maxHeight: null,
            focus: true
        });
    });
</script>
</body>
</html>

