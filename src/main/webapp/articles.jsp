<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="Portal a'la Wiki zrealizowany przgrupę SDA_Team1"/>
    <meta name="keywords" content="ala wiki"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css">

    <meta charset="UTF-8">
    <style type="text/css">
        .filterable {
            width: 100%;
            margin-top: 15px;
        }

        .filterable .panel-heading .pull-right {
            margin-top: -20px;
        }

        .filterable .filters input[disabled] {
            background-color: transparent;
            border: none;
            cursor: auto;
            box-shadow: none;
            padding: 0;
            height: auto;
        }

        .filterable .filters input[disabled]::-webkit-input-placeholder {
            color: #333;
        }

        .filterable .filters input[disabled]::-moz-placeholder {
            color: #333;
        }

        .filterable .filters input[disabled]:-ms-input-placeholder {
            color: #333;
        }

    </style>
</head>
<body>
<div class="container">  <!-- container -->
    <c:set var="pageName" value="articles"/> <%-- TU WYPELNIC NAZWE PODSTRONY --%>
    <jsp:include page="menu.jsp">
        <jsp:param name="pageName" value="${pageName}"/>
    </jsp:include>
    <div class="jumbotron text-center">
        <div class="container">
            <h1 class="display-4">Artykuły</h1> <%-- TU DODAC TYTUL PODSTRONY --%>
            <p class="lead">Tutaj znajdziesz wszystkie artykuły</p> <%-- TU UZUPELNIC KROTKI OPIS --%>
        </div>
    </div>
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-tabela-tab" data-toggle="tab" href="#nav-tabela" role="tab"
               aria-controls="nav-tabela" aria-selected="true">Tabelarycznie</a>
            <a class="nav-item nav-link" id="nav-kartki-tab" data-toggle="tab" href="#nav-kartki" role="tab"
               aria-controls="nav-kartki" aria-selected="false">Kartki</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-tabela" role="tabpanel" aria-labelledby="nav-tabela-tab">
            <%--pierwsza zakładka--%>
            <div class="container">
                <div class="row">
                    <div class="panel panel-primary filterable">
                        <div class="card-header">
                            <div class="pull-left">
                                <button class="btn btn-default btn-xs btn-filter"><span
                                        class="glyphicon glyphicon-filter"></span>
                                    Filtruj...
                                </button>
                            </div>
                        </div>
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr class="filters">
                                <th style="width: 80%">
                                    <input type="text" class="form-control" placeholder="Tytył artykułu" disabled>
                                </th>
                                <th>
                                    <input type="text" class="form-control" placeholder="Kategoria" disabled>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${sessionScope.get('ArticleRepository').getArticles()}" var="articles">
                                <c:set value="${sessionScope.get('CategoryRepository').getById(articles.idcategory)}"
                                       var="category"/>
                                <tr>
                                    <td><a href="article.jsp?id=${articles.id}">${articles.title}</a>
                                        <small><p class="card-text">${sessionScope.get('ArticleRepository').getPartContent(articles)}</p></small>
                                    </td>
                                    <td>${category.categoryname}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <%--pierwsza zakładka--%>

        </div>
        <div class="tab-pane fade" id="nav-kartki" role="tabpanel" aria-labelledby="nav-kartki-tab">
            <%--druga zakładka--%>
            <br>
            <div class="container">
                <div class="row">
                    <%--<div class="card-group">--%>
                        <%--class="card-group"--%>
                        <c:forEach items="${sessionScope.get('ArticleRepository').getArticles()}" var="articles">
                        <c:set value="${sessionScope.get('CategoryRepository').getById(articles.idcategory)}"
                               var="category"/>
                        <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">${articles.title}</h5>
                                <h6 class="card-subtitle mb-2 text-muted">${category.categoryname}</h6>
                                <p class="card-text">${sessionScope.get('ArticleRepository').getPartContent(articles)}</p>
                                <a href="article.jsp?id=${articles.id}" class="card-link">Otwórz</a>
                            </div>
                        </div>
                    </div>
                    </c:forEach>
                <%--</div>--%>
            </div>
        </div>
        <%--druga zakładka--%>
    </div>
</div>
<%-- Zawartość strony --%>

<%-- /Zawartość strony --%>
<jsp:include page="footer.jsp"/>
</div> <!-- /container -->
<script type="text/javascript">

    $(document).ready(function () {
        $('.filterable .btn-filter').click(function () {
            var $panel = $(this).parents('.filterable'),
                $filters = $panel.find('.filters input'),
                $tbody = $panel.find('.table tbody');
            if ($filters.prop('disabled') == true) {
                $filters.prop('disabled', false);
                $filters.first().focus();
            } else {
                $filters.val('').prop('disabled', true);
                $tbody.find('.no-result').remove();
                $tbody.find('tr').show();
            }
        });

        $('.filterable .filters input').keyup(function (e) {
            /* Ignore tab key */
            var code = e.keyCode || e.which;
            if (code == '9') return;
            /* Useful DOM data and selectors */
            var $input = $(this),
                inputContent = $input.val().toLowerCase(),
                $panel = $input.parents('.filterable'),
                column = $panel.find('.filters th').index($input.parents('th')),
                $table = $panel.find('.table'),
                $rows = $table.find('tbody tr');
            /* Dirtiest filter function ever ;) */
            var $filteredRows = $rows.filter(function () {
                var value = $(this).find('td').eq(column).text().toLowerCase();
                return value.indexOf(inputContent) === -1;
            });
            /* Clean previous no-result if exist */
            $table.find('tbody .no-result').remove();
            /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
            $rows.show();
            $filteredRows.hide();
            /* Prepend no-result row if all rows are filtered */
            if ($filteredRows.length === $rows.length) {
                $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="' + $table.find('.filters th').length + '">No result found</td></tr>'));
            }
        });
    });
</script>
</body>
</html>