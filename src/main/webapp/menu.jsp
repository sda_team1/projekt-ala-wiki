<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>

<c:set var="pageName" value="<%=request.getParameter("pageName")%>"/>

<c:set var="isLogin" value="<%=request.getSession().getAttribute("isLogin")%>"/>
<c:set var="firstName" value="<%=request.getSession().getAttribute("firstName")%>"/>
<c:set var="accessLevel" value="<%=request.getSession().getAttribute("accessLevel")%>"/>

<%--Diagnostyka--%>
<%--<c:out value="GET: ${pageName}"></c:out>--%>

<div class="header clearfix">
    <nav>
        <ul class="nav nav-pills float-right">
            <c:choose>
                <c:when test="${pageName eq 'index'}">
                    <li class="nav-item">
                        <a class="nav-link active" href="index.jsp">Początek <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="articles.jsp">Artykuły</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.jsp">O nas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact.jsp">Kontakt</a>
                    </li>
                </c:when>
                <c:when test="${pageName eq 'about'}">
                    <li class="nav-item">
                        <a class="nav-link" href="index.jsp">Początek <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="articles.jsp">Artykuły</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="about.jsp">O nas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact.jsp">Kontakt</a>
                    </li>
                </c:when>
                <c:when test="${pageName eq 'articles'}">
                    <li class="nav-item">
                        <a class="nav-link" href="index.jsp">Początek <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="articles.jsp">Artykuły</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.jsp">O nas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact.jsp">Kontakt</a>
                    </li>
                </c:when>
                <c:when test="${pageName eq 'contact'}">
                    <li class="nav-item">
                        <a class="nav-link" href="index.jsp">Początek <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="articles.jsp">Artykuły</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.jsp">O nas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="contact.jsp">Kontakt</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="nav-item">
                        <a class="nav-link" href="index.jsp">Początek <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="articles.jsp">Artykuły</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.jsp">O nas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact.jsp">Kontakt</a>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
    </nav>
    <c:if test="${isLogin eq 'true'}">
    <h3 class="text-muted">Witaj: ${firstName}! <a href="<%= application.getContextPath() %>/logout">
        <button type="button" class="btn btn-warning">Wyloguj</button></a>

        <c:if test="${accessLevel eq 'admin'}">
        <a href="adminpage.jsp">
            <button type="button" class="btn btn-danger">Admin</button>
        </a>
        </c:if>
    </c:if>
        <c:if test="${empty isLogin}">
        <h3 class="text-muted">
            <a href="login.jsp">
                <button type="button" class="btn btn-success">Zaloguj</button>
            </a></h3>
        </c:if>
</div>